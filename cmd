# CMD BOT
/speedtest: bash /root/TELEXWRT/menu.sh speedtest
/vnstat: bash /root/TELEXWRT/menu.sh vnstat
/clear: bash /root/TELEXWRT/menu.sh clear
/ping: bash /root/TELEXWRT/menu.sh ping
/reboot: bash /root/TELEXWRT/menu.sh reboot
/shutdown: bash /root/TELEXWRT/shutdown.sh shutdown
/system: bash /root/TELEXWRT/menu.sh system
/restart: bash /root/TELEXWRT/menu.sh restart
/vssrstart: bash /root/TELEXWRT/menu.sh vssrstart
/vssrstop: bash /root/TELEXWRT/menu.sh vssrstop
/vssrrestart: bash /root/TELEXWRT/menu.sh vssrrestart

/shadowsocksrstart: bash /root/TELEXWRT/menu.sh shadowsocksrstart
/shadowsocksrstop: bash /root/TELEXWRT/menu.sh shadowsocksrstop
/shadowsocksrrestart: bash /root/TELEXWRT/menu.sh shadowsocksrrestart
